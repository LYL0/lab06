package lab06;
/*Louisa Yuxin Lin 1933472*/
import java.util.*;
public class RpsGame {
	private int wintimes;
	private int tietimes;
	private int losetimes;
	Random ran = new Random();
	public RpsGame() {
		this.wintimes=0;
		this.tietimes=0;
		this.losetimes=0;
		
	}
	public int getWin() {
		return this.wintimes;
	}
	public int getTie() {
		return this.tietimes;
	}
	public int getLose() {
		return this.losetimes;
	}
	public static void main (String[] args) {
		String choice ="scissors";
		RpsGame game = new RpsGame();
		String result=game.playRound(choice);
		System.out.println(result + "You had lost "+ game.getLose() +" times.");
		
	}
	
	public String playRound(String user) {
		
	//	This method randomly choose a symbol for the computer
		int computerChoice=ran.nextInt(3);
		String choice="";
		String winner="";
		if(computerChoice==0) {
			choice="rock";
		}
		if(computerChoice==1) {
			choice="scissors";
		}
		if (computerChoice==2) {
			choice="paper";
		}
		//0 stands for rock,1 stands for scissors, 2 stands for paper
		
		if(choice.equals("scissors") && user.equals("paper")) {
			losetimes++;
			winner = "the computer";
		}
		else if(choice.equals("rock") && user.equals("scissors")) {
			losetimes++;
			winner = "the computer";
		}
		else if(choice.equals("paper") && user.equals("rock")) {
			losetimes++;
			winner = "the computer";
		}
		else if(choice.equals("paper") && user.equals("scissors")) {
			wintimes++;
			winner = "You";
		}
		else if(choice.equals("scissors") && user.equals("rock")) {
			wintimes++;
			winner = "You";
		}
		else if(choice.equals("rock") && user.equals("paper")) {
			wintimes++;
			winner = "You";
		}
		else {
			tietimes++;
			winner = "Nobody";
		}
		return "Computer plays " + computerChoice + " and " + winner + " won!!";	
	} 
}
