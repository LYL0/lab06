/*Louisa Yuxin Lin 1933472*/
package lab06;

	import javafx.application.*;
	import javafx.event.ActionEvent;
	import javafx.event.EventHandler;
	import javafx.stage.*;
	import javafx.scene.*;
	import javafx.scene.paint.*;
	import javafx.scene.control.*;
	import javafx.scene.layout.*;


	public class RpsApplication extends Application {	
		private RpsGame g = new RpsGame();
		public void start(Stage stage) {
			Group root = new Group(); 
			VBox general = new VBox();
			//button
			HBox buttons = new HBox();
			Button rockB = new Button ("Rock");
			rockB.setPrefSize(110,50);
			Button scissorB = new Button ("Scissors");
			scissorB.setPrefSize(110,50);
			Button paperB = new Button ("Paper");
			paperB.setPrefSize(110,50);
			buttons.getChildren().addAll(rockB,scissorB,paperB);
			buttons.setPrefHeight(80);
			//text fields
			HBox textFields = new HBox();
			TextField messages = new TextField();
			messages.setPrefSize(300, 50);
			TextField wins = new TextField("Wins: 0");
			wins.setPrefSize(90,50);
			TextField losses = new TextField("Losses: 0");
			losses.setPrefSize(90,50);
			TextField ties = new TextField("Ties: 0");
			ties.setPrefSize(90,50);
			textFields.getChildren().addAll(messages,wins,losses,ties);
			
			//adding elements to vbox & root
			general.getChildren().addAll(buttons,textFields);
			root.getChildren().add(general);
			
			//setting what happens when each button is clicked
			rockB.setOnAction(new RpsChoice(messages, wins, losses, ties, "rock", g));
			scissorB.setOnAction(new RpsChoice(messages, wins, losses, ties, "scissors", g));
			paperB.setOnAction(new RpsChoice(messages, wins, losses, ties, "paper", g));
			

			
			//scene is associated with container, dimensions
			Scene scene = new Scene(root, 650, 300); 
			scene.setFill(Color.BLACK);

			//associate scene to stage and show
			stage.setTitle("Rock Paper Scissors"); 
			stage.setScene(scene); 
			stage.show(); 
		}
		
	    public static void main(String[] args) {
	        Application.launch(args);
	    }
	}    


