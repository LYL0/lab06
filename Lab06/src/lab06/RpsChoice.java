/*Louisa Yuxin Lin 1933472*/
package lab06;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
	private RpsApplication app;
	private TextField messages;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	public RpsChoice(TextField messages, TextField wins, TextField losses, TextField ties, String choice, RpsGame game) {
		this.messages= messages;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.game = game;
	}
		@Override
	public void handle(ActionEvent e) {
		// TODO Auto-generated method stub
			String result = game.playRound(choice);
			messages.setText(result);
			losses.setText("Losses: " + game.getLose());
			ties.setText("Ties: " + game.getTie());
			wins.setText("Wins: " + game.getWin());

	}
}
